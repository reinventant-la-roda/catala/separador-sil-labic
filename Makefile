help:
	@echo "getResources: Get examples from dictionary"
	@echo "build:        Compiles the splitter"
	@echo "run:          tmp"

getResources:
	./gradlew :getExamples:run

build:
	./gradlew :separador:build

run:
	./gradlew :separador:run

.PHONY: help\
	getResources \
	build \
