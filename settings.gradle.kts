rootProject.name = "syllabic.splitter"

include(":getExamples")
include(":separador")

dependencyResolutionManagement {
    versionCatalogs {
        create("testLibs") {
            version("jupiter", "5.9.0")
            version("kotest", "5.4.1")
            library("jupiter-api", "org.junit.jupiter", "junit-jupiter").versionRef("jupiter")
            library("jupiter-run", "org.junit.jupiter", "junit-jupiter-engine").versionRef("jupiter")
            library("kotest-core", "io.kotest", "kotest-property").versionRef("kotest")
            library("kotest-assertion", "io.kotlintest", "kotlintest-assertions").versionRef("kotest")
            bundle("testing", listOf("jupiter-api", "kotest-core"))
        }
    }
}
