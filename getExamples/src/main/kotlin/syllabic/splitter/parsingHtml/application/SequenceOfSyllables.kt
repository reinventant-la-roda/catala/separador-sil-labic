package syllabic.splitter.parsingHtml.application

import syllabic.domain.Word
import syllabic.splitter.parsingHtml.domain.Repository

class SequenceOfSyllables(repository: Repository) : Iterator<List<Word>> {
    private val words = repository.getSyllables()

    override fun hasNext(): Boolean = words.hasNext()

    override fun next(): List<Word> {
        val list = words.next()
        println("TODO logs: list")
        return list
    }
}

fun sequenceOfSyllables(repository: Repository) = iterator<List<Word>> {
    val words = repository.getSyllables()
    while (words.hasNext()) {
        val list = words.next()
        println("TODO logs: list")
        yield(list)
    }
}
