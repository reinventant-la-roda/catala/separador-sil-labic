package syllabic.splitter.parsingHtml.infrastructure

import it.skrape.core.htmlDocument
import it.skrape.fetcher.HttpFetcher
import it.skrape.fetcher.extract
import it.skrape.fetcher.skrape
import it.skrape.selects.eachLink
import it.skrape.selects.eachText
import it.skrape.selects.html5.a
import it.skrape.selects.html5.table
import it.skrape.selects.html5.tr
import syllabic.domain.Word
import syllabic.splitter.parsingHtml.domain.Repository
import java.io.File

const val BASE_URL = "http://ca.oslin.org"
val MATCH_REGEX = Regex("""(\S++)\s++\([^)]++\)\s++(\S++)""")

class SkrapeitRepository : Repository {

    override fun getSyllables(): Iterator<List<Word>> = parseAllWebSite()

    private fun parseAllWebSite() = iterator<List<Word>> {
        for (letter in 'a'..'z') {
            var url: String? = "$BASE_URL/syllables.php?act=list&letter=$letter"

            while (url != null) {
                val lofc = parsingHtml(url)
                url = lofc.next
                println(url)
                yield(lofc.words)
            }
        }
    }

    private fun parsingHtml(url: String): Lofc = skrape(HttpFetcher) {
        request { this.url = url }

        extract {
            htmlDocument {
                Lofc(
                    words =
                    table {
                        withAttribute = "id" to "rollovertable"
                        tr {
                            findAll {
                                eachText
                                    .drop(1)
                                    .map(String::toWord)
                            }
                        }
                    },
                    next = a { findAll { eachLink["següents"] } }?.let { "$BASE_URL$it" }
                )
            }
        }
    }

    fun parseFile(file: File) = htmlDocument(file) {
        table {
            withAttribute = "id" to "rollovertable"
            tr {
                findAll {
                    eachText
                        .drop(1)
                        .map(String::toWord)
                }
            }
        }
    }

}

private fun String.toWord(): Word {
    val (name, syllable) = MATCH_REGEX
        .matchEntire(this)
        ?.destructured
        ?: return Word.of("-$this", listOf("error", "of", "parsing"))
    return Word.of(name, syllable.split("·"))
}

// Lèxic Obert Flexionat de Català
private data class Lofc(val words: List<Word>, val next: String?)
