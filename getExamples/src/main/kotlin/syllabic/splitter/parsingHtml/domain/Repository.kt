package syllabic.splitter.parsingHtml.domain

import syllabic.domain.Word

interface Repository {
    fun getSyllables(): Iterator<List<Word>>
}
