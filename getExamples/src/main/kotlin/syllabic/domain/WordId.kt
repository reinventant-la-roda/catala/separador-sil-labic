package syllabic.domain

import kotlinx.serialization.Serializable

@Serializable
data class WordId(val name: String) {
    override fun toString(): String = name
}
