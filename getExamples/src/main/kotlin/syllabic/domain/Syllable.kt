package syllabic.domain

import kotlinx.serialization.Serializable

@Serializable
data class Syllable(val name: String) {
    override fun toString(): String = name
}
