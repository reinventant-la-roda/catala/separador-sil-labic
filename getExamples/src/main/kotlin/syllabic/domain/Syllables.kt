package syllabic.domain

import kotlinx.serialization.Serializable

@Serializable
data class Syllables(val syllables: List<Syllable>) {

    override fun toString(): String = syllables.joinToString("-")

    companion object {
        // XXX: https://stackoverflow.com/questions/56384970/kotlin-cant-create-two-constructors-with-different-list-types-parameters
        // java there exists something called type erasure. And because kotlin uses the JVM it is also affected by this limitation
        fun of(syllables: List<String>) = syllables.map(::Syllable).let(::Syllables)
    }
}
