package syllabic.domain

import kotlinx.serialization.*

@Serializable
data class Word(val id: WordId, val syllables: Syllables) {
    constructor(pair : Pair<String, List<String>>) : this(pair.first.let(::WordId), pair.second.let(Syllables::of))

    override fun toString(): String = "$id: $syllables"

    companion object {
        fun of(id: String, syllables: List<String>) = Word(Pair(id, syllables))
    }
}
