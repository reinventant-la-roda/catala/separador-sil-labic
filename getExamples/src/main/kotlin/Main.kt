import syllabic.splitter.parsingHtml.application.SequenceOfSyllables
import syllabic.splitter.parsingHtml.infrastructure.SkrapeitRepository
import java.io.File

fun main() {
    val readRepo = SequenceOfSyllables(SkrapeitRepository())
    val file = File("silabes")
    file.printWriter().use { out ->
        readRepo
            .forEach { list ->
                list.forEach(out::println)
            }
    }
}
