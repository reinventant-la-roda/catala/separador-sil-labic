package syllabic.domain

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class SyllablesTest {

    @Test
    fun `check toString`() {
        val syllables = listOf("pro", "gra", "ma", "dor")
            .let(Syllables::of)

        syllables.toString() shouldBe "pro-gra-ma-dor"
    }
}
