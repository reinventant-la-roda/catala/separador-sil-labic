package syllabic.domain

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class WordTest {
    @Test
    fun `check toString`() {
        val word = Word.of("pencaire", listOf("pen", "cai", "re"))

        word.toString() shouldBe "pencaire: pen-cai-re"
    }
}
