package syllabic.splitter.parsingHtml.infrastructure

import io.kotest.matchers.shouldBe
import it.skrape.core.htmlDocument
import it.skrape.selects.eachText
import it.skrape.selects.html5.table
import it.skrape.selects.html5.tr
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.io.File

class SkrapeitRepositoryTest {

    @Test
    fun parseFile() {
        val repository = SkrapeitRepository()
        val file = File(this.javaClass.getResource("/syllabic/splitter/lofcDiccionarySillabic.html")!!.path)

        val result = repository.parseFile(file)

        result.size shouldBe 100
        result.distinct().size shouldBe 85
    }

    @Nested
    inner class howUseSkrapeit {
        private val html = """
            <table id="theTable">
                <tbody>
                    <tr><th>Words</th><th>Syllabic</th></tr>
                    <tr><td title='word'><a>baador</a> (adjectiu) </td><td title='words'>ba<b>·</b>a<b>·</b><u><b>dor</b></u></td></tr>
                    <tr><td title='word'><a>baador</a> (nom) </td><td title='words'>ba<b>·</b>a<b>·</b><u><b>dor</b></u></td></tr>
                    <tr><td title='word'><a>baboiada</a> (nom) </td><td title='words'>ba<b>·</b>bo<b>·</b><u><b>ia</b></u><b>·</b>da</td></tr>
                </tbody>
            </table>
            """.trimIndent()

        @Test
        fun skrapeitWithRegex() {
            htmlDocument(html) {
                table {
                    withAttribute = "id" to "theTable"
                    tr {
                        findAll {
                            eachText
                                .drop(1)
                                .map {
                                    val (key, value) = Regex("""(^\S++)\s++\([^)]++\)\s++(\S++)$""")
                                        .matchEntire(it)!!
                                        .destructured
                                    Pair(key, value.split("·"))
                                }
                        }
                    }
                }
            }
                .forEach(::println)
        }

        @Test
        fun skrapeitWithZipFile() {
            htmlDocument(html) {
                "table[id='theTable'] tr" {
                    "td[title='word'] a" { findAll { eachText } }.zip(
                        "td[title='words']" { findAll { eachText } }.map { it.split("·") }
                    )
                }
            }
                .forEach(::println)
        }
    }
}
