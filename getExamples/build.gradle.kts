plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    application
}

group = "syllabic.splitter"
version = "0.0.0-SNAPSHOT"

val scrape = "1.1.5"
val serialization = "1.3.3"

dependencies {
    implementation("it.skrape:skrapeit:$scrape")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serialization")
    testImplementation(testLibs.bundles.testing)
}

tasks.test {
    useJUnitPlatform()
}

application {
    mainClass.set("MainKt")
}
