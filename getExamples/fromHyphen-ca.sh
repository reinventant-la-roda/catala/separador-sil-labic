#!/bin/bash -xe

curl https://raw.githubusercontent.com/jaumeortola/hyphen-ca/master/mots-separats.txt \
	| perl -pe 's/(?<key>\S++) (\S++ )?--> \S++ (?<value>\S++)/$+{key},$+{value}/' \
	> "${1}"
