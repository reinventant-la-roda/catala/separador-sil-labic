plugins {
    kotlin("jvm")
    application
}

dependencies {
    //testImplementation(testLibs.bundles.testing)
    //testImplementation(testLibs.kotest.assertion)

    testImplementation(testLibs.jupiter.api)
    testImplementation(testLibs.jupiter.run)
    testImplementation("io.kotest:kotest-runner-junit5:4.6.1")
    testImplementation("io.kotest:kotest-assertions-core:4.6.1")
    testImplementation("io.kotest:kotest-assertions-json:4.6.1")
    testImplementation("io.kotest:kotest-property:4.6.1")
    testImplementation("io.kotest:kotest-framework-datatest:4.6.1")
}

tasks.test {
    useJUnitPlatform()
}

application {
    mainClass.set("MainKt")
}
