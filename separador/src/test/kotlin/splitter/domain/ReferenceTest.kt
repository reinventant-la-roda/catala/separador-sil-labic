package splitter.domain

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.string.shouldContain
import org.junit.jupiter.api.Test

class ReferenceTest {

    @Test
    fun words() {
        "quadrícula"
            .map(Reference::of)
            .shouldContainExactly(
                Reference.CONSONANT, // q
                Reference.SEMIVOWEL, // u
                Reference.VOWEL,     // a
                Reference.CONSONANT, // d
                Reference.CONSONANT, // r
                Reference.VOWEL,     // í
                Reference.CONSONANT, // c
                Reference.SEMIVOWEL, // u
                Reference.CONSONANT, // l
                Reference.VOWEL,     // a
            )
    }

    @Test
    fun `not expected`() {
        val char = '_'
        val exception: WhatReferenceIs = shouldThrow {
            Reference.of(char)
        }

        exception.message shouldContain "'$char'"
    }
}
