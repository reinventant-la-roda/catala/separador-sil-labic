package splitter.domain

import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.*
import parse.implementation.ReadFile

val TWO_CONSONANTS_REGEX = Regex("""[${Reference.getConsonants()}]{2}""")
val TWO_VOWELS_REGEX = Regex("""[${Reference.getVowels()}]{2}""")

class SyllabicSplitterTest {
    private val syllabicExamples: Sequence<Pair<String, List<String>>> by lazy {
        ReadFile.getFromHyphenCa().asSequence()
    }

    @Test
    fun unaVocalUnaConsonant() {
        val (works, errors) = syllabicExamples
            .filter { Regex("""^[${Reference.getConsonants()}${Reference.getVowels()}]++$""").containsMatchIn(it.first) }
            .filterNot { TWO_CONSONANTS_REGEX.containsMatchIn(it.first) }
            .filterNot { TWO_VOWELS_REGEX.containsMatchIn(it.first) }
            .filterNot { it.first.startsWith("an") }
            .filterNot { it.first.startsWith("ben") }
            .filterNot { it.first.startsWith("bes") }
            .filterNot { it.first.startsWith("cap") }
            .filterNot { it.first.startsWith("co") }
            .filterNot { it.first.startsWith("des") }
            .filterNot { it.first.startsWith("en") }
            .filterNot { it.first.startsWith("ex") }
            .filterNot { it.first.startsWith("hiper") }
            .filterNot { it.first.startsWith("in") }
            .filterNot { it.first.startsWith("mal") }
            .filterNot { it.first == "napicol" }
            .filterNot { it.first == "panamericà" }
            .filterNot { it.first.startsWith("per") }
            .filterNot { it.first == "rebesavi" }
            .filterNot { it.first.startsWith("sub") }
            .filterNot { it.first.startsWith("super") }
            .partition { SyllabicSplitter.of(it.first).splitList == it.second }

        println("Works with a total of ${works.size}!")
        println("Not work for a total of ${errors.size}")

        errors.forEach(::println)
        errors.shouldBeEmpty()
    }

    @TestFactory
    fun ups() = listOf(
        Pair("acotar", "a-co-tar"),
        //Pair("anisocitosi", "an-i-so-ci-to-si"),
        Pair("Adam", "A-dam"),
        Pair("ceŀla", "cel-la"),
        Pair("cel·la", "cel-la"),
        Pair("CEĿLA", "CEL-LA"),
        Pair("tanqueu", "tan-queu"),
        Pair("liqüeu", "li-qüeu"),
        Pair("veieu", "ve-ieu"),
        Pair("aguait", "a-guait"),
    ).map(::dynamicTestWordToSplit)

    // https://sites.google.com/a/insvilafant.cat/llengua-catalana-3r-d-eso/unitat-1/ortografia-diftongs-triftongs-i-hit
    @Nested
    inner class LlenguaCatalana3erEso {

        @Nested
        inner class DiftongCreixent {
            @TestFactory
            fun gq() = listOf(
                Pair("llengua", "llen-gua"),
                Pair("llengües", "llen-gües"),
                Pair("paraigua", "pa-rai-gua"),
                Pair("pasqües", "pas-qües"),
                Pair("pingüí", "pin-güí"),
                Pair("quota", "quo-ta"),
                Pair("qüestió", "qües-ti-ó"),
            ).map(::dynamicTestWordToSplit)

            @Test
            fun beginningGJ() {
                SyllabicSplitter("llegua")
                    .stepByStep
                    .first { it.first == SplitSteps.JoinBeginningGJ }
                    .second
                    .shouldContainExactly(
                        Change(
                            index = 4,
                            from = SyllabicState.UNKNOWN,
                            to = SyllabicState.JOIN
                        )
                    )
            }

            @Test
            fun debilEntreFortes() = listOf(
                Pair("cauen", "ca-uen"),
                Pair("iaio", "ia-io"),
                Pair("joia", "jo-ia"),
                Pair("noies", "no-ies"),
            ).map(::dynamicTestWordToSplit)

            @TestFactory
            fun comencamentParaula() = listOf(
                Pair("hiat", "hiat"),
                Pair("hiena", "hie-na"),
                Pair("hieràtic", "hie-rà-tic"),
                Pair("iaia", "ia-ia"),
                Pair("iarda", "iar-da"),
                Pair("iode", "io-de"),
                Pair("ioga", "io-ga"),
                Pair("iogurt", "io-gurt"),
                Pair("iot", "iot"),
            ).map(::dynamicTestWordToSplit)
        }

        @TestFactory
        fun diftongDecreixent() = listOf(
            Pair("blau", "blau"),
            Pair("ciutat", "ciu-tat"),
            Pair("couràs", "cou-ràs"),
            Pair("cuiner", "cui-ner"),
            Pair("duus", "duus"),
            Pair("mai", "mai"),
            Pair("neu", "neu"),
            Pair("noi", "noi"),
            Pair("novii", "no-vii"),
            Pair("remei", "re-mei"),
        ).map(::dynamicTestWordToSplit)


        @TestFactory
        fun triftong() = listOf(
            Pair("creieu", "cre-ieu"),
            Pair("creueu", "cre-ueu"),
            Pair("guaita", "guai-ta"),
            Pair("Paraguai", "Pa-ra-guai"),
        ).map(::dynamicTestWordToSplit)

        @TestFactory
        fun hiat() = listOf(
            Pair("diada", "di-a-da"),
            Pair("farmàcia", "far-mà-ci-a"),
            Pair("oïda", "o-ï-da"),
            Pair("pèrdua", "pèr-du-a"),
            Pair("raïm", "ra-ïm"),
            Pair("teatre", "te-a-tre"),
        ).map(::dynamicTestWordToSplit)
    }

    // https://www.iec.cat/llengua/documents/ortografia_catalana_versio_digital.pdf#page=102
    @Nested
    inner class IecCat {
        @TestFactory // a)
        fun consonantAbansVocal() = listOf(
            Pair("capacitat", "ca-pa-ci-tat"),
            Pair("govern", "go-vern"),
            Pair("neboda", "ne-bo-da"),
            Pair("rigorosament", "ri-go-ro-sa-ment"),
            Pair("tela", "te-la"),
        ).map(::dynamicTestWordToSplit)

        @TestFactory // b)
        fun entreVocals() = listOf(
            Pair("acte", "ac-te"),
            Pair("activitat", "ac-ti-vi-tat"),
            Pair("adjunt", "ad-junt"),
            Pair("adstrat", "ads-trat"),
            Pair("altre", "al-tre"),
            Pair("cabra", "ca-bra"),
            Pair("comptable", "comp-ta-ble"),
            Pair("costat", "cos-tat"),
            Pair("gàngster", "gàngs-ter"),
            Pair("herba", "her-ba"),
            Pair("infernal", "in-fer-nal"),
            Pair("instrument", "ins-tru-ment"),
            Pair("llàgrima", "llà-gri-ma"),
            Pair("monstre", "mons-tre"),
            Pair("nucli", "nu-cli"),
            Pair("pedra", "pe-dra"),
            Pair("problema", "pro-ble-ma"),
            Pair("reflex", "re-flex"),
            Pair("replicar", "re-pli-car"),
            Pair("secretari", "se-cre-ta-ri"),
            Pair("segle", "se-gle"),
            Pair("sofrir", "so-frir"),
            Pair("substantiu", "subs-tan-tiu"),
            Pair("suprem", "su-prem"),
        ).map(::dynamicTestWordToSplit)

        @TestFactory // c)
        fun diftongsTriftongs() = listOf(
            Pair("claudàtor", "clau-dà-tor"),
            Pair("creuar", "cre-uar"),
            Pair("creueu", "cre-ueu"),
            Pair("delinqüent", "de-lin-qüent"),
            Pair("fèieu", "fè-ieu"),
            Pair("guaita", "guai-ta"),
            Pair("lingüística", "lin-güís-ti-ca"),
            Pair("neula", "neu-la"),
            Pair("noia", "no-ia"),
            Pair("obliqüeu", "o-bli-qüeu"),
        ).map(::dynamicTestWordToSplit)

        @TestFactory // d)
        fun digrafsSeparats() = listOf(
            Pair("cotna", "cot-na"),
            Pair("despatxar", "des-pat-xar"),
            Pair("espatlla", "es-pat-lla"),
            Pair("jutge", "jut-ge"),
            Pair("llotja", "llot-ja"),
            Pair("Novetlè", "No-vet-lè"),
            Pair("piscina", "pis-ci-na"),
            Pair("potser", "pot-ser"),
            Pair("queixa", "quei-xa"),
            Pair("ràtzia", "ràt-zi-a"),
            Pair("setmana", "set-ma-na"),
            Pair("tassa", "tas-sa"),
            Pair("terra", "ter-ra"),
        ).map(::dynamicTestWordToSplit)

        @TestFactory // e)
        fun lgeminada() = listOf(
            Pair("coŀlegi", "col-le-gi"),
            Pair("novel·la", "no-vel-la"),
        ).map(::dynamicTestWordToSplit)

        @TestFactory // f)
        fun digrafsJunts() = listOf(
            Pair("orgue", "or-gue"),
            Pair("palla", "pa-lla"),
            Pair("Puigcerdà", "Puig-cer-dà"),
            Pair("quinqué", "quin-qué"),
            Pair("renyar", "re-nyar"),
            Pair("sikhisme", "si-khis-me"),
        ).map(::dynamicTestWordToSplit)

        @TestFactory // g)
        fun palatoalveolar() = listOf(
            Pair("alcohol", "al-co-hol"),
            Pair("boxa", "bo-xa"),
            Pair("extern", "ex-tern"),
            Pair("leishmaniosi", "leish-ma-ni-o-si"),
            Pair("texans", "te-xans"),
            Pair("tothom", "tot-hom"),
        ).map(::dynamicTestWordToSplit)

        @Disabled("Need origin of words")
        @TestFactory
        fun prefixos() = listOf(
            Pair("anovulatori", "an-ovulatori"),
            Pair("benestant", "ben-estant"),
            Pair("besavi", "bes-avi"),
            Pair("cisalpi", "cis-alpí"),
            Pair("coordinador", "co-ordinador"),
            Pair("desorganitzar", "des-organitzar"),
            Pair("exalumne", "ex-alumne"),
            Pair("hiperestàtic", "hiper-estàtic"),
            Pair("hiperrealisme", "hiper-realisme"),
            Pair("inoblidable", "in-oblidable"),
            Pair("interregne", "inter-regne"),
            Pair("interurbà", "inter-urbà"),
            Pair("paneslavisme", "pan-eslavisme"),
            Pair("postoperatori", "post-operatori"),
            Pair("sotsoficial", "sots-oficial"),
            Pair("sotssíndic", "sots-síndic"),
            Pair("subaltern", "sub-altern"),
            Pair("subratllar", "sub-ratllar"),
            Pair("superrealista", "super-realista"),
            Pair("transatlàntic", "trans-atlàntic"),
            Pair("transsexual", "trans-sexual"),
        ).map(::dynamicTestWordToSplit)

        @Disabled("Need origin of words")
        @TestFactory
        fun radicals() = listOf(
            Pair("celobert", "cel-obert"),
            Pair("Duesaigües", "Dues-aigües"),
            Pair("gentilhome", "gentil-home"),
            Pair("nosaltres", "nos-altres"),
            Pair("vosaltres", "vos-altres"),
        ).map(::dynamicTestWordToSplit)
    }

    private fun dynamicTestWordToSplit(input: Pair<String, String>) = dynamicTestWordToSplit(input.first, input.second)
    private fun dynamicTestWordToSplit(input: String, expected: String): DynamicTest? =
        DynamicTest.dynamicTest("$input to $expected") {
            SyllabicSplitter.of(input).splitList shouldBe expected.split("-")
        }
}
