package parse.implementation

import java.io.File

typealias Split = Pair<String, List<String>>

private val parseTwoDotsRegex = Regex("""^([^:]++): (\S++)$""")
private val parseCvsRegex = Regex("""^([^,]++),([^,]++)$""")

class ReadFile(private val file: File) {

    fun read(parseRegex: Regex, split: Char): List<Split> =
        file.useLines { sequence -> sequence.map { parse(it, parseRegex, split) }.toList() }

    companion object {
        fun fromResources(resource: String): ReadFile =
            javaClass.classLoader.getResource(resource)?.path?.let(::File)?.let(::ReadFile)
                ?: throw Exception("Not exist $resource")

        // http://ca.oslin.org/syllables.php
        fun getFromCaOslinOrg(): List<Split> = fromResources("silabes.txt").read(parseTwoDotsRegex, '-')

        // https://github.com/jaumeortola/hyphen-ca/blob/master/mots-separats.txt
        fun getFromHyphenCa(): List<Split> = fromResources("mots-separats.txt").read(parseCvsRegex, '_')
    }

    private fun parse(line: String, parseRegex: Regex, split: Char): Split {
        val (word, preList) = parseRegex.find(line)!!.destructured

        return Pair(word, preList.split(split))
    }
}
