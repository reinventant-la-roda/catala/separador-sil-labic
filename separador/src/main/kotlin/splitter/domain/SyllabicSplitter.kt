package splitter.domain

private val consonantRegex = """[${Reference.getConsonants()}]"""
private val vowelRegex = """[${Reference.getVowels()}]"""
private val weakVowelRegex = """[${Reference.getWeakVowels()}]"""
private val strongVowelRegex = """[${Reference.getStrongVowels()}]"""

enum class SplitSteps {
    JoinBeginningGJ,
    JoinBeginningWord,
    WeakVowelBetweenStrong,
    Hiatus,
    WeakAfterStrong,
    JoinConsonantBeforeVowel,
    JoinTwoConsonants,
    SplitFlyingPoint,
    SplitConsonantBetweenVowels,
    JoinConsonantsAfterVowel,
    JoinConsonantToVowel,
}

typealias ChangeValue = Pair<SplitSteps, List<Change>>

data class Change(val index: Int, val to: SyllabicState) {
    constructor(index: Int, from: SyllabicState, to: SyllabicState) : this(index, to) {
        if (from != SyllabicState.UNKNOWN)
            throw Exception("ups")
    }
}

// https://www.iec.cat/llengua/documents/ortografia_catalana_versio_digital.pdf#page=102
class SyllabicSplitter(inputWord: String) {
    private val word: String = inputWord.lGem()
    private val syllabicStates: MutableList<SyllabicState> = word.toUnknownState().toMutableList()
    private val stepByStepMutable: MutableList<ChangeValue> =
        mutableListOf()

    init {
        splitBySteps()
    }

    val splitList: List<String> by lazy(::toStringList)
    val splitString: String by lazy(::toString)
    val stepByStep: List<ChangeValue> by lazy { stepByStepMutable.toList() }

    private fun splitBySteps(): SyllabicSplitter =
        twoOrMoreVowelsTogether()
            .joinConsonantBeforeVowel()
            .joinTwoConsonants()
            .splitFlyingPoint()
            .splitConsonantBetweenVowels()
            .joinConsonantsAfterVowel()
            .joinConsonantToVowel()

    private fun twoOrMoreVowelsTogether(): SyllabicSplitter =
        joinBeginningWithGJ()
            .joinBeginningWord()
            .weakVowelBetweenStrong()
            .hiatus()
            .weakAfterStrongVowel()

    private fun step(step: SplitSteps, modify: () -> Unit): SyllabicSplitter {
        val previous = this.syllabicStates.toList()

        modify()

        (previous zip this.syllabicStates)
            .withIndex()
            .filter { (_, values) -> values.first != values.second }
            .let { changes ->
                if (changes.isNotEmpty()) stepByStepMutable +=
                    Pair(step, changes.map { (index, values) -> Change(index, values.first, values.second) })
            }

        return this
    }

    private fun joinBeginningWithGJ(): SyllabicSplitter = step(SplitSteps.JoinBeginningGJ) {
        """[gq][uü]$strongVowelRegex"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { match -> match.range.first + 1 }
            .forEach { index -> syllabicStates[index] = SyllabicState.JOIN }
    }

    private fun joinBeginningWord(): SyllabicSplitter = step(SplitSteps.JoinBeginningWord) {
        """^h?$weakVowelRegex$strongVowelRegex"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .forEach { _ ->
                if (word.startsWith("h", ignoreCase = true))
                    syllabicStates[1] = SyllabicState.JOIN
                syllabicStates[0] = SyllabicState.JOIN
            }
    }

    private fun weakVowelBetweenStrong(): SyllabicSplitter = step(SplitSteps.WeakVowelBetweenStrong) {
        """$strongVowelRegex$weakVowelRegex$strongVowelRegex"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { it.range.first }
            .forEach { index ->
                syllabicStates[index] = SyllabicState.SPLIT
                syllabicStates[index + 1] = SyllabicState.JOIN
            }
    }

    private fun hiatus(): SyllabicSplitter = step(SplitSteps.Hiatus) {
        """$vowelRegex$strongVowelRegex"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { it.range.first }
            .filter { index -> syllabicStates[index] == SyllabicState.UNKNOWN }
            .forEach { index -> syllabicStates[index] = SyllabicState.SPLIT }
    }

    private fun weakAfterStrongVowel(): SyllabicSplitter = step(SplitSteps.WeakAfterStrong) {
        """$vowelRegex$weakVowelRegex"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { it.range.first }
            .filter { index -> syllabicStates[index] == SyllabicState.UNKNOWN }
            .forEach { index -> syllabicStates[index] = SyllabicState.JOIN }
    }

    private fun joinConsonantBeforeVowel(): SyllabicSplitter = step(SplitSteps.JoinConsonantBeforeVowel) {
        """$consonantRegex$vowelRegex"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { it.range.first }
            .filter { index -> syllabicStates[index] == SyllabicState.UNKNOWN }
            .forEach { index -> syllabicStates[index] = SyllabicState.JOIN }
    }

    private fun joinTwoConsonants(): SyllabicSplitter = step(SplitSteps.JoinTwoConsonants) {
        """([bcfgp][lr]|[dt]r|ll|ny|kh)"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { match -> match.range.first }
            .filter { index -> syllabicStates[index] == SyllabicState.UNKNOWN }
            .forEach { index -> syllabicStates[index] = SyllabicState.JOIN }
    }

    // Aquí no pot arribar el caràcter ŀ!
    private fun splitFlyingPoint(): SyllabicSplitter = step(SplitSteps.SplitFlyingPoint) {
        """l·"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { match -> match.range }
            .forEach { range -> range.forEach { index -> syllabicStates[index] = SyllabicState.SPLIT } }
    }

    private fun splitConsonantBetweenVowels(): SyllabicSplitter = step(SplitSteps.SplitConsonantBetweenVowels) {
        """$vowelRegex$consonantRegex++(?=$vowelRegex)""" // Last vowel need to stay in (?=) because we want to match all consonants between vowels
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { match -> match.range }
            .filter { range -> SyllabicState.SPLIT !in syllabicStates.slice(range) }
            .forEach { range ->
                range
                    .last { index -> syllabicStates[index] == SyllabicState.UNKNOWN }
                    .let { index -> syllabicStates[index] = SyllabicState.SPLIT }
            }
    }

    private fun joinConsonantsAfterVowel(): SyllabicSplitter = step(SplitSteps.JoinConsonantsAfterVowel) {
        """$vowelRegex$consonantRegex++"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { match -> match.range.first until match.range.last }
            .forEach { range ->
                range
                    .takeWhile { index -> syllabicStates[index] == SyllabicState.UNKNOWN }
                    .forEach { index -> syllabicStates[index] = SyllabicState.JOIN }
            }
    }

    private fun joinConsonantToVowel(): SyllabicSplitter = step(SplitSteps.JoinConsonantToVowel) {
        """^$consonantRegex++(?=$vowelRegex)"""
            .toRegex(RegexOption.IGNORE_CASE)
            .findAll(word)
            .map { match -> match.range }
            .forEach { range -> range.forEach { index -> syllabicStates[index] = SyllabicState.JOIN } }
    }

    private fun toStringList(): List<String> {
        if (syllabicStates.any { it == SyllabicState.UNKNOWN })
            throw Exception("ups with: '$word' $this")

        val splits = syllabicStates.mapIndexed { index, syllabicState -> Pair(index, syllabicState) }
            .filter { it.second == SyllabicState.SPLIT }
            .map { it.first }

        return (listOf(-1) + splits + listOf(word.length - 1))
            .zipWithNext { a, b -> word.substring(a + 1..b) }
            .filterNot { Reference.of(it.first()) == Reference.SPLIT }
    }

    override fun toString(): String =
        syllabicStates.zip(word.toList()) { s: SyllabicState, c: Char ->
            "$c$s"
        }.joinToString(separator = "") + word.last()

    companion object {
        fun of(word: String) = word.let(::SyllabicSplitter)
    }
}


private fun String.toUnknownState(): List<SyllabicState> = this.zipWithNext { _, _ -> SyllabicState.UNKNOWN }
private fun String.lGem(): String = this
    .replace("ŀ", "l·")
    .replace("Ŀ", "L·")
