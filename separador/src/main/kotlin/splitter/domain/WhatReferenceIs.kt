package splitter.domain

class WhatReferenceIs(c: Char): Exception("What reference is it that value? '$c'")
