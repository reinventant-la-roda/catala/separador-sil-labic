package splitter.domain

enum class SyllabicState {
    UNKNOWN {
        override fun toString(): String = "?"
    },
    JOIN {
        override fun toString(): String = "_"
    },
    SPLIT {
        override fun toString(): String = "-"
    }
}
