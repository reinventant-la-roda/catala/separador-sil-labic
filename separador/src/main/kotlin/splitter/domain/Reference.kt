package splitter.domain

enum class Reference(val type: String, val values: String) {
    VOWEL("vocal", "aàeéèíïoóòú"),
    SEMIVOWEL("semivocal", "iuü"),
    CONSONANT("consonant", "bcçdfghjklmnpqrstvwxyz"),
    SPLIT("puntuació", "·-");

    companion object {
        fun of(c: Char) = Reference
            .values()
            .firstOrNull { c.lowercase() in it.values }
            ?: throw WhatReferenceIs(c)

        fun getVowels() = VOWEL.values + SEMIVOWEL.values
        fun getWeakVowels() = SEMIVOWEL.values
        fun getStrongVowels() = VOWEL.values
        fun getConsonants() = CONSONANT.values
    }
}
